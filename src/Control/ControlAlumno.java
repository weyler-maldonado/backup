/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package Control;

/**
 *
 * @author weyler
 */
import Main.NewMain;
import Modelo.Alumno;
import Vista.Registro;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ControlAlumno implements ActionListener {

    Log log = LogFactory.getLog(ControlAlumno.class);

    private Alumno modeloAlumno;
    private Registro vista;
    ArrayList<Alumno> agregados = new ArrayList<>();

    public ControlAlumno(Alumno modeloAlumno, Registro vista) {
        this.modeloAlumno = modeloAlumno;
        this.vista = vista;

        this.vista.jBAgregar.addActionListener(this);
        this.vista.jBActualizar.addActionListener(this);
        this.vista.jBEliminar.addActionListener(this);
        this.vista.jBReply.addActionListener(this);

    }
    /*
    *Inicia componentes de la GUI
    */
    public void initComponents(boolean visible) {
        vista.setTitle("Registro Alumnos");
        vista.setLocationRelativeTo(null);
        vista.setResizable(false);
        vista.setVisible(visible);
    }
    /**
    *Limpia elementos de la tabla
    */
    public void limpiarCampos() {
        vista.jTxFNombre.setText("");
        vista.jTeFMatricula.setText("");
        vista.jTxFEdad.setText("");
        vista.jTxFCarrera.setText("");
    }
    
    /**
    *Establece conexión en la base de datos 'Test'
    */
    public ConnectionSource conexion() {
        try {
            String databaseUrl;
            ConnectionSource fuente;
            
           
                databaseUrl = "jdbc:postgresql://localhost:5432/Test";
                String contraseña = "postgres";
                fuente = new JdbcConnectionSource(databaseUrl, "postgres", contraseña);
//                TableUtils.createTableIfNotExists(fuente, Alumno.class);
            
            return fuente;
        } catch (Exception e) {
            System.out.println("Error de conexion");
            return null;
        }
    }
    
    /**
    *Evento accionado por el botón Agregar
    */
    public void agregarDatos() {
        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);
            

            if (datosLlenados()) {

                String nombre, matricula, carrera;
                int edad;
                nombre = vista.jTxFNombre.getText();
                matricula = vista.jTeFMatricula.getText();
                edad = Integer.parseInt(vista.jTxFEdad.getText());
                carrera = vista.jTxFCarrera.getText();
                Alumno alumno = new Alumno(nombre, matricula, edad, carrera);
                agregados.add(alumno);
                alumnoDao.create(alumno);
                mostrar();
                limpiarCampos();
                
                 String queryInsert = "INSERT INTO alumnos (nombre, matricula, edad, carrera)" + "[" + nombre + ", " + matricula + ", "
                         + edad + ", " + carrera + "]";
                 
                 log.debug(queryInsert);
                
                connectionSource.close();
            } else {
                JOptionPane.showMessageDialog(null, "Por favor, llene todos los campos");
            }

           
        } catch (Exception e) {
            System.out.println("No se pudo conectar" + e);

        }

    }
    
    /**
    *Verifica si todos los campos de la tabla están llenos
    */
    public boolean datosLlenados() {
        Boolean nombre = vista.jTxFNombre.getText().isEmpty();
        Boolean matricula = vista.jTeFMatricula.getText().isEmpty();
        Boolean edad = vista.jTxFEdad.getText().isEmpty();
        Boolean carrera = vista.jTxFCarrera.getText().isEmpty();
        if (nombre != true && matricula != true && edad != true && carrera != true) {
            return true;
        } else {
            return false;
        }
    }
    /**
    *Muestra los elementos en la tabla de la GUI
    */
    public void mostrar() {

        Object[][] matriz = new Object[agregados.size()][4];
        for (int i = 0; i < agregados.size(); i++) {
            matriz[i][0] = agregados.get(i).getNombre();
            matriz[i][1] = agregados.get(i).getMatricula();
            matriz[i][2] = agregados.get(i).getEdad();
            matriz[i][3] = agregados.get(i).getCarrera();

        }
        DefaultTableModel modelo = new DefaultTableModel(matriz, new String[]{"Nombre", "Matricula", "Edad", "Carrera"});
        vista.Tabla.setModel(modelo);

    }
    /**
    *Evento accionado por el botón Actualizar
    */
    public void actualizarDatos() {

        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);

            DefaultTableModel modelo = (DefaultTableModel) vista.Tabla.getModel();
            int seleccionado = vista.Tabla.getSelectedRow();

            if (seleccionado < 0) {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un Registro");
            } else if (datosLlenados()) {

                String nombreSelect = vista.Tabla.getValueAt(seleccionado, 0).toString();

                String nombre = vista.jTxFNombre.getText();
                String matricula = vista.jTeFMatricula.getText();
                int edad = Integer.parseInt(vista.jTxFEdad.getText());
                String carrera = vista.jTxFCarrera.getText();

                for (int i = 0; i < agregados.size(); i++) {
                    if (nombreSelect.equals(agregados.get(i).getNombre())) {
                        System.out.println("entra");
                        agregados.get(i).setNombre(nombre);
                        agregados.get(i).setMatricula(matricula);
                        agregados.get(i).setEdad(edad);
                        agregados.get(i).setCarrera(carrera);
                        modelo.setValueAt(agregados.get(i).getNombre(), i, 0);
                        modelo.setValueAt(agregados.get(i).getMatricula(), i, 1);
                        modelo.setValueAt(agregados.get(i).getEdad(), i, 2);
                        modelo.setValueAt(agregados.get(i).getCarrera(), i, 3);
                        alumnoDao.update(agregados.get(i));
                      

                    }
                }

                JOptionPane.showMessageDialog(null, "Registro actualizado");
                limpiarCampos();
                connectionSource.close();
            } else {
                JOptionPane.showMessageDialog(null, "Debe llenar todos los datos");
            }

            /*for (int i = 0; i < agregados.size(); i++) {
                System.out.println("Alumnos , " + agregados.get(i).toString());
            }*/
        } catch (Exception e) {
            System.out.println("Eror de conexion");
        }

    }
    /**
    *Evento accionado por el botón Agregar, creo tuplas en la base de datos
    */
    public void recuperarDatos() {

        try {

            ConnectionSource connectionSource = conexion();
           
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);
           System.out.println("Conectado");
            for (Alumno alumno : alumnoDao) {
                if (alumno.getNombre() != null) {
                    agregados.add(alumno);
                    mostrar();
                    System.out.println(alumno.getNombre());
                } else {
                    System.out.println("BD vacia");
                }

            }

            connectionSource.close();

        } catch (Exception e) {
            System.out.println("Error de conexion");

        }

    }
/**
    *Evento accionado por el botón Eliminar, seleccionando una tupla de la tabla de GUI
    */
    public void eliminarDatos() {
        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);

            DefaultTableModel modelo = (DefaultTableModel) vista.Tabla.getModel();
            int seleccionado = vista.Tabla.getSelectedRow();
            String nombre;
            if (seleccionado < 0) {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un Registro");
            } else {

                nombre = vista.Tabla.getValueAt(seleccionado, 0).toString();

                for (int i = 0; i < agregados.size(); i++) {
                    if (nombre.equals(agregados.get(i).getNombre())) {
                        alumnoDao.delete(agregados.get(i));
                        agregados.remove(i);
                        modelo.removeRow(seleccionado);
                   
                    }
                }
               

                JOptionPane.showMessageDialog(null, "Registro eliminado");

            }
            connectionSource.close();

            /*for (int i = 0; i < agregados.size(); i++) {
                System.out.println("Alumnos , " + agregados.get(i).toString());
            }*/
        } catch (Exception e) {
            System.out.println("Error de conexion");

        }

    }
    /**
    *Evento accionado por el botón Reply, tranfiere la información del archivo registro.log que se realizó durante la ejecución
    * de la aplicación a una base de datos de respaldo
    */
    public void replyDB(){
        try {
            PrintWriter writer;
            writer = new PrintWriter("registro.log");
            writer.print("");
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControlAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        Alumno modeloAlumno = new Alumno();
        Registro vista = new Registro();
        ControlAlumno controlador = new ControlAlumno(modeloAlumno, vista);
        controlador.conexion();
        controlador.initComponents(false);
        controlador.recuperarDatos();
        
        Alumno a =  new Alumno();
        Registro r =  new Registro();
        AuxControl aux =  new AuxControl(a,r);
        aux.initComponents();
    }
   /**
    *Eventos nativos del ActionListener
    */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.jBAgregar) {
            agregarDatos();
        }
        if (ae.getSource() == vista.jBActualizar) {
            actualizarDatos();
        }
        if (ae.getSource() == vista.jBEliminar) {
            eliminarDatos();

        }
        if (ae.getSource() == vista.jBReply) {
            
            replyDB();
            JOptionPane.showMessageDialog(null, "BD cambiada");
        }
    }

}
