package Control;

import Modelo.Alumno;
import Vista.Registro;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AuxControl implements ActionListener {
    
    private Alumno modeloAlumno;
    private Registro vista;
    ArrayList<Alumno> agregados = new ArrayList<>();
    String[] aux = new String[5];
    int cont = 0;

    public AuxControl(Alumno modeloAlumno, Registro vista) {
        this.modeloAlumno = modeloAlumno;
        this.vista = vista;

        this.vista.jBAgregar.addActionListener(this);
        this.vista.jBActualizar.addActionListener(this);
        this.vista.jBEliminar.addActionListener(this);
        this.vista.jBReply.addActionListener(this);

    }

    public void initComponents() {
        vista.setTitle("Registro Alumnos");
        vista.setLocationRelativeTo(null);
        vista.setResizable(false);
        vista.setVisible(true);
        realizarCambio();
    }

    private void limpiarCampos() {
        vista.jTxFNombre.setText("");
        vista.jTeFMatricula.setText("");
        vista.jTxFEdad.setText("");
        vista.jTxFCarrera.setText("");
    }
    
    public void recuperarDatos() {
        try {

            ConnectionSource connectionSource = conexion();

            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);
            System.out.println("Conectado");
            for (Alumno alumno : alumnoDao) {
                if (alumno.getNombre() != null) {
                    agregados.add(alumno);
                    mostrar();
                    System.out.println(alumno.getNombre());
                } else {
                    System.out.println("BD vacia");
                }

            }

            connectionSource.close();

        } catch (Exception e) {
            System.out.println("Error de conexion");

        }

    }
    /**
    *Establece conexión con la base de datos de respaldo
    */
    public ConnectionSource conexion() {
        try {
            String databaseUrl;
            ConnectionSource fuente;

            databaseUrl = "jdbc:postgresql://localhost:5432/Aux";
            String contraseña = "postgres";
            fuente = new JdbcConnectionSource(databaseUrl, "postgres", contraseña);
//            TableUtils.createTableIfNotExists(fuente, Alumno.class);s
            
            return fuente;
            
        } catch (Exception e) {
            System.out.println("Error de conexion");
            return null;
        }
    }

    public void agregarDatos() {
        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);

            if (datosLlenados()) {

                String nombre, matricula, carrera;
                int edad;
                nombre = vista.jTxFNombre.getText();
                matricula = vista.jTeFMatricula.getText();
                edad = Integer.parseInt(vista.jTxFEdad.getText());
                carrera = vista.jTxFCarrera.getText();
                Alumno alumno = new Alumno(nombre, matricula, edad, carrera);
                agregados.add(alumno);
                alumnoDao.create(alumno);
                mostrar();
                limpiarCampos();

//                  String query = alumnoDao.g
//                        System.out.println("QUERY" + query);
                connectionSource.close();
            } else {
                JOptionPane.showMessageDialog(null, "Por favor, llene todos los campos");
            }

            /*for (int i = 0; i < agregados.size(); i++) {
                System.out.println("Alumnos , " + agregados.get(i));
            }*/
        } catch (Exception e) {
            System.out.println("No se pudo conectar" + e);

        }

    }

    private boolean datosLlenados() {
        Boolean nombre = vista.jTxFNombre.getText().isEmpty();
        Boolean matricula = vista.jTeFMatricula.getText().isEmpty();
        Boolean edad = vista.jTxFEdad.getText().isEmpty();
        Boolean carrera = vista.jTxFCarrera.getText().isEmpty();
        if (nombre != true && matricula != true && edad != true && carrera != true) {
            return true;
        } else {
            return false;
        }
    }

    private void mostrar() {

        Object[][] matriz = new Object[agregados.size()][4];
        for (int i = 0; i < agregados.size(); i++) {
            matriz[i][0] = agregados.get(i).getNombre();
            matriz[i][1] = agregados.get(i).getMatricula();
            matriz[i][2] = agregados.get(i).getEdad();
            matriz[i][3] = agregados.get(i).getCarrera();

        }
        DefaultTableModel modelo = new DefaultTableModel(matriz, new String[]{"Nombre", "Matricula", "Edad", "Carrera"});
        vista.Tabla.setModel(modelo);

    }

    public void actualizarDatos() {

        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);

            DefaultTableModel modelo = (DefaultTableModel) vista.Tabla.getModel();
            int seleccionado = vista.Tabla.getSelectedRow();

            if (seleccionado < 0) {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un Registro");
            } else if (datosLlenados()) {

                String nombreSelect = vista.Tabla.getValueAt(seleccionado, 0).toString();

                String nombre = vista.jTxFNombre.getText();
                String matricula = vista.jTeFMatricula.getText();
                int edad = Integer.parseInt(vista.jTxFEdad.getText());
                String carrera = vista.jTxFCarrera.getText();

                for (int i = 0; i < agregados.size(); i++) {
                    if (nombreSelect.equals(agregados.get(i).getNombre())) {
                        System.out.println("entra");
                        agregados.get(i).setNombre(nombre);
                        agregados.get(i).setMatricula(matricula);
                        agregados.get(i).setEdad(edad);
                        agregados.get(i).setCarrera(carrera);
                        modelo.setValueAt(agregados.get(i).getNombre(), i, 0);
                        modelo.setValueAt(agregados.get(i).getMatricula(), i, 1);
                        modelo.setValueAt(agregados.get(i).getEdad(), i, 2);
                        modelo.setValueAt(agregados.get(i).getCarrera(), i, 3);
                        alumnoDao.update(agregados.get(i));

                    }
                }

                JOptionPane.showMessageDialog(null, "Registro actualizado");
                limpiarCampos();
                connectionSource.close();
            } else {
                JOptionPane.showMessageDialog(null, "Debe llenar todos los datos");
            }

            /*for (int i = 0; i < agregados.size(); i++) {
                System.out.println("Alumnos , " + agregados.get(i).toString());
            }*/
        } catch (Exception e) {
            System.out.println("Eror de conexion");
        }

    }

    private void eliminarDatos() {
        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);

            DefaultTableModel modelo = (DefaultTableModel) vista.Tabla.getModel();
            int seleccionado = vista.Tabla.getSelectedRow();
            String nombre;
            if (seleccionado < 0) {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un Registro");
            } else {

                nombre = vista.Tabla.getValueAt(seleccionado, 0).toString();

                for (int i = 0; i < agregados.size(); i++) {
                    if (nombre.equals(agregados.get(i).getNombre())) {
                        alumnoDao.delete(agregados.get(i));
                        agregados.remove(i);
                        modelo.removeRow(seleccionado);

                    }
                }

                JOptionPane.showMessageDialog(null, "Registro eliminado");

            }
            connectionSource.close();

            /*for (int i = 0; i < agregados.size(); i++) {
                System.out.println("Alumnos , " + agregados.get(i).toString());
            }*/
        } catch (Exception e) {
            System.out.println("Error de conexion");

        }

    }
    /**
    *Lee el archivo registro.log, y busca de línea en línea verificando si es una query a ejecutar
    * @param String
    */
    private void evaluarLog(String linea) {
        
        if(linea.contains("FieldType assiging from data class Modelo.Alumno")) {
            cont++;
            String[] split = linea.split(" ");
            
            aux[cont - 1] = split[12];
            
            if(cont == 5) {
                cont = 0;
                exportarDatos(Integer.valueOf(aux[0]), aux[1], aux[2], Integer.valueOf(aux[3]), aux[4]);
                aux = new String[5];  
             
            }
        }
//        
//        if(linea.contains("BaseMappedStatement insert arguments")) {        
//            String[] split = linea.split(":");
//            String[] split1 = split[3].split(", ");
//            String[] split2 = split1[0].split(Pattern.quote("["));
//            String[] split3 = split1[4].split(Pattern.quote("]"));
//            
//            exportarDatos(Integer.valueOf(split2[1]), split1[1], split1[2], Integer.valueOf(split1[3]), split3[0]);
//        }
    }
    /**
    *Lee el archivo y va enviando línea por línea a evaluarLog()
    */
    private void realizarCambio() {
        Scanner fileIn; 
        String nombreArchivo, linea;
        nombreArchivo = "registro.log";
       
        try {
            
            fileIn = new Scanner(new FileReader (nombreArchivo));
            
            while(fileIn.hasNextLine()){
                linea = fileIn.nextLine();
                evaluarLog(linea);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("not found");
        }
    }
    /**
    *Recibe los datos enviados por evaluarLog() y lo mete en la tabla de la base de datos de respaldo
    */
    
        private void exportarDatos(int id, String nombre, String matricula, int edad, String carrera) {
        try {

            ConnectionSource connectionSource = conexion();
            Dao<Alumno, Integer> alumnoDao = DaoManager.createDao(connectionSource, Alumno.class);
            
            Alumno alumno = new Alumno(id, nombre, matricula, edad, carrera);
            agregados.add(alumno);
            alumnoDao.create(alumno);
            mostrar();
            limpiarCampos();

            connectionSource.close();

        } catch (Exception e) {
            System.out.println("No se pudo conectar" + e);

        }
    }

    public void replyDB() {
        Alumno a =  new Alumno();
        Registro r =  new Registro();
        vista.setVisible(false);
        ControlAlumno alum =  new ControlAlumno(a,r);
        alum.initComponents(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.jBAgregar) {
            agregarDatos();

        }
        if (ae.getSource() == vista.jBActualizar) {
            actualizarDatos();
        }
        if (ae.getSource() == vista.jBEliminar) {
            eliminarDatos();

        }
        if (ae.getSource() == vista.jBReply) {
            replyDB();
            JOptionPane.showMessageDialog(null, "BD cambiada");
        }
    }

}
