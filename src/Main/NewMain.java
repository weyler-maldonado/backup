package Main;

import Control.ControlAlumno;
import Modelo.Alumno;
import Vista.Registro;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class NewMain {

    public static void main(String[] args) throws SQLException{
        
        Alumno modeloAlumno = new Alumno();
        Registro vista = new Registro();
        ControlAlumno controlador = new ControlAlumno(modeloAlumno, vista);
          
        controlador.conexion();

        controlador.initComponents(true);
        controlador.recuperarDatos();
        /**
        *Thread para limpiar la base de datos de respaldo, para así, evitar colision en una segunda ejecución de la aplicación
        */
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                try {
                    Connection cn = enableConnection();

                    Statement st = cn.createStatement();
                    st.executeUpdate("DELETE FROM Alumnos *");

                    st.close();
                    disableConnection(cn);
                }
                catch(SQLException e) {
                    
                }
            
            }

        });
    }
    
    /**
     *Enable connection with database
     */
    public static Connection enableConnection() 
    {
        Connection connect = null;
        
        try 
        {
            Class.forName("org.postgresql.Driver");
            connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Aux", "postgres", "postgres");
        } 
        catch (ClassNotFoundException | SQLException ex) 
        {
            System.out.println("Error " + ex);
        }
        
        return connect;
    }
    
    /**
     *Disable connection with database
     */
    public static void disableConnection(Connection cn) 
    {
        try 
        {
            cn.close();
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al cerrar coneccion " + ex);
        }
    }
    

    
}
