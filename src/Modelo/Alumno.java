package Modelo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable (tableName = "alumnos")
public class Alumno {
    
    public static final String ID = "_id";
    public static final String NOMBRE = "nombre";
    public static final String MATRICULA = "matricula";
    public static final String EDAD = "edad";
    public static final String CARRERA = "carrera";
    
    @DatabaseField(generatedId = true, columnName = ID) 
    private int id;
    @DatabaseField(columnName = NOMBRE)
    private String nombre;
    @DatabaseField(columnName = MATRICULA)
    private String matricula;
    @DatabaseField(columnName = EDAD)
    private int edad;
    @DatabaseField(columnName = CARRERA)
    private String carrera;

    public Alumno(String nombre, String matricula, int edad, String carrera) {
        this.nombre = nombre;
        this.matricula = matricula;
        this.edad = edad;
        this.carrera = carrera;
    }

    public Alumno(int id, String nombre, String matricula, int edad, String carrera) {
        this.id = id;
        this.nombre = nombre;
        this.matricula = matricula;
        this.edad = edad;
        this.carrera = carrera;
    }
    
    /**
     *Constructor requerido por ORM
     */
    public Alumno() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
  
    @Override
    public String toString() {
        return "Alumno{" + "nombre=" + nombre + ", matricula=" + matricula + ", edad=" + edad + ", carrera=" + carrera + '}';
    }
    
}
